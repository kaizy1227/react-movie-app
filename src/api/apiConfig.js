const apiConfig = {
    baseUrl: 'https://api.themoviedb.org/3/',
    apiKey: '0d1394bd77d70a55f0ef63883700d349',
    originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`
}

export default apiConfig;